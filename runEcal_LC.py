#!/usr/bin/env python
"""Run tests on G4FaserAlgConfigNew

Copyright (C) 2002-2021 CERN for the benefit of the ATLAS and FASER collaborations
"""

if __name__ == '__main__':

    import time
    a = time.time()

#
# Set up logging and config behaviour
#
    from AthenaCommon.Logging import log
    from AthenaCommon.Constants import DEBUG
    from AthenaCommon.Configurable import Configurable
    log.setLevel(DEBUG)
    Configurable.configurableRun3Behavior = 1
#
# Import and set config flags
#
    from CalypsoConfiguration.AllConfigFlags import ConfigFlags
    ConfigFlags.Exec.MaxEvents = 4  # can be overridden from command line with --maxEvt=<number>
    ConfigFlags.Exec.SkipEvents = 0 # can be overridden from command line with --skipEvt=<number>
    from AthenaConfiguration.Enums import ProductionStep
    ConfigFlags.Common.ProductionStep = ProductionStep.Simulation
### Add in Radial Position Sampler into FaserParticleGunConfig

#
# All these must be specified to avoid auto-configuration
#
    ConfigFlags.Input.RunNumber = [12345] #Isn't updating - todo: investigate
    ConfigFlags.Input.OverrideRunNumber = True
    ConfigFlags.Input.LumiBlockNumber = [1]
    ConfigFlags.Input.isMC = True
#
# Output file name
# 
    ConfigFlags.Output.HITSFileName= "localtest.pool.root"
# can be overridden from command line with Output.HITSFileName=<name>
#
# Sim ConfigFlags
#
    ConfigFlags.Sim.Layout = "FASER"
    ConfigFlags.Sim.PhysicsList = "FTFP_BERT"
    ConfigFlags.Sim.ReleaseGeoModel = False
    ConfigFlags.Sim.IncludeParentsInG4Event = True # Controls whether BeamTruthEvent is written to output HITS file
    ConfigFlags.addFlag("Sim.Gun",{"Generator" : "SingleParticle"})  # Property bag for particle gun keyword:argument pairs

    #ConfigFlags.GeoModel.FaserVersion = "FASERNU-02"             # Geometry set-up
    #ConfigFlags.GeoModel.FaserVersion = "FASER-01"
 # FASER-01 Previous Ecal Version
    #ConfigFlags.GeoModel.FaserVersion = "FASER-02" 
 # FASER-02 Current Ecal Version with Preshower Material
    ConfigFlags.GeoModel.FaserVersion = "FASER-TB00"
 # FASER-TB00 Geo for Test Beam
    ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-TB00"
    #ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-02"             # Conditions set-up
    #ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-01"
    ConfigFlags.addFlag("Input.InitialTimeStamp", 0)             # To avoid autoconfig 
    ConfigFlags.GeoModel.Align.Dynamic = False
#
# Override flags above from command line
#
    import sys
    ConfigFlags.fillFromArgs(sys.argv[1:])
#
# By being a little clever, we can steer the geometry setup from the command line using GeoModel.FaserVersion
#
# Start with minimal configuration for Testbeam
#
    detectors = ['Veto', 'Preshower', 'FaserSCT', 'Ecal']
    if ConfigFlags.GeoModel.FaserVersion.count("TB") == 0 :
        detectors += ['Trigger', 'Dipole']
    if ConfigFlags.GeoModel.FaserVersion.count("FASERNU") > 0 :
        detectors += ['Emulsion']
#
# Setup detector flags
#
    from CalypsoConfiguration.DetectorConfigFlags import setupDetectorsFromList
    setupDetectorsFromList(ConfigFlags, detectors, toggle_geometry=True)
#
# Finalize flags
#
    ConfigFlags.lock()
#
# Initialize a new component accumulator
#
    from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
    cfg = MainServicesCfg(ConfigFlags)
#
# Check whether a real input file was specified
#
    if ConfigFlags.Input.Files and ConfigFlags.Input.Files != ["_CALYPSO_GENERIC_INPUTFILE_NAME_"] :
        print("Input.Files = ",ConfigFlags.Input.Files)
#
# If so, set up to read it
#
        from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
        cfg.merge(PoolReadCfg(ConfigFlags))
#
# If not, configure the particle gun as requested, or using defaults
#
    else :
#
# Particle gun generators - the generator, energy, angle, particle type, position, etc can be modified by passing keyword arguments
#
        from FaserParticleGun.FaserParticleGunConfig import FaserParticleGunCfg
### add radial pos sampler here?

###
        cfg.merge(FaserParticleGunCfg(ConfigFlags))
        from McEventSelector.McEventSelectorConfig import McEventSelectorCfg
        cfg.merge(McEventSelectorCfg(ConfigFlags))
#
# Output file
#
    from AthenaPoolCnvSvc.PoolWriteConfig import PoolWriteCfg
    cfg.merge(PoolWriteCfg(ConfigFlags))
#
# Add the G4FaserAlg
#
    from G4FaserAlg.G4FaserAlgConfigNew import G4FaserAlgCfg
    cfg.merge(G4FaserAlgCfg(ConfigFlags))
#
# Dump config
#
    from AthenaConfiguration.ComponentFactory import CompFactory
    cfg.addEventAlgo(CompFactory.JobOptsDumperAlg(FileName="G4FaserTestConfig.txt"))
    cfg.getService("StoreGateSvc").Dump = True
    cfg.getService("ConditionStore").Dump = True
    cfg.printConfig(withDetails=True, summariseProps = False)  # gags on ParticleGun if summariseProps = True?

    ConfigFlags.dump()
    f = open("test.pkl","wb")
    cfg.store(f)
    f.close()
#
# Execute and finish
#
    sc = cfg.run()

    b = time.time()
    log.info("Run G4FaserAlg in " + str(b-a) + " seconds")
#
# Success should be 0
#
    sys.exit(not sc.isSuccess())

